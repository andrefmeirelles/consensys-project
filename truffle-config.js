const path = require('path')
const HDWalletProvider = require('truffle-hdwallet-provider')
// ganache wallet - DO NOT USE IN PRODUCTION
const mnemonic = 'veteran cube come urge fold crack mouse kitchen civil service dry mad'
// address '0: 0xfd3cdf502c122688b96ee8945a3ba0948b1c65a9

module.exports = {
    contracts_build_directory: path.join(__dirname, 'client/src/contracts'),
    solc: {
        optimizer: {
            enabled: true,
            runs: 200
        }
    },
    networks: {
        development: {
            host: "localhost",
            port: 8545,
            network_id: "*", // Match any network id
            gas: 4712388
        },
        ropsten: {
            provider: function() {
                return new HDWalletProvider(mnemonic, 'https://ropsten.infura.io/v3/e3e816616c254e49a31def82d694b452')
            },
            network_id: 3,
            gas: 4699999,
            gasPrice: 3000000000
        }
    }
};
