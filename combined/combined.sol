pragma solidity ^0.5.8;

/**
 * @dev Contract module which provides a basic access control mechanism, where
 * there is an account (an owner) that can be granted exclusive access to
 * specific functions.
 *
 * This module is used through inheritance. It will make available the modifier
 * `onlyOwner`, which can be aplied to your functions to restrict their use to
 * the owner.
 */
contract Ownable {
    address private _owner;

    event OwnershipTransferred(address indexed previousOwner, address indexed newOwner);

    /**
     * @dev Initializes the contract setting the deployer as the initial owner.
     */
    constructor () internal {
        _owner = msg.sender;
        emit OwnershipTransferred(address(0), _owner);
    }

    /**
     * @dev Returns the address of the current owner.
     */
    function owner() public view returns (address) {
        return _owner;
    }

    /**
     * @dev Throws if called by any account other than the owner.
     */
    modifier onlyOwner() {
        require(isOwner(), "Ownable: caller is not the owner");
        _;
    }

    /**
     * @dev Returns true if the caller is the current owner.
     */
    function isOwner() public view returns (bool) {
        return msg.sender == _owner;
    }

    /**
     * @dev Leaves the contract without owner. It will not be possible to call
     * `onlyOwner` functions anymore. Can only be called by the current owner.
     *
     * > Note: Renouncing ownership will leave the contract without an owner,
     * thereby removing any functionality that is only available to the owner.
     */
    function renounceOwnership() public onlyOwner {
        emit OwnershipTransferred(_owner, address(0));
        _owner = address(0);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     * Can only be called by the current owner.
     */
    function transferOwnership(address newOwner) public onlyOwner {
        _transferOwnership(newOwner);
    }

    /**
     * @dev Transfers ownership of the contract to a new account (`newOwner`).
     */
    function _transferOwnership(address newOwner) internal {
        require(newOwner != address(0), "Ownable: new owner is the zero address");
        emit OwnershipTransferred(_owner, newOwner);
        _owner = newOwner;
    }
}

/**
 * @title An ERC-20 emulator, allowing ERC-20 enabled wallets to interact with
 * an asset manager contract.
 */
contract ERC20Emulator {

    Game public game;
    uint256 public assetId;

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev The constructor sets the game and assetId, which cannot be changed
     * @param _game the asset manager contract (in this project, a game)
     * @param _assetId the asset to be wrapped by this contract
     */
    constructor(address payable _game, uint256 _assetId) public {
        game = Game(_game);
        assetId = _assetId;
    }

    /**
     * @dev Emulates the ERC-20 name property
     * @return string the asset name
     */
    function name() public view returns (string memory) {
        return game.name(assetId);
    }

    /**
     * @dev Emulates the ERC-20 symbol property
     * @return string the asset symbol
     */
    function symbol() public view returns (string memory) {
        return game.symbol(assetId);
    }

    /**
     * @dev Emulates the ERC-20 decimals property
     * @return uint8 the asset decimals
     */
    function decimals() public view returns (uint8) {
        return game.decimals(assetId);
    }

    /**
     * @dev Emulates the ERC-20 totalSupply property
     * @return uint256 the asset totalSupply
     */
    function totalSupply() public view returns (uint256) {
        return game.totalSupply(assetId);
    }

    /**
     * @dev Gets the balance of a wallet in the asset manager
     * @param _owner the wallet to be checked
     * @return uint256 the balance
     */
    function balanceOf(address _owner) public view returns (uint256 balance) {
        return game.balanceOf(assetId, _owner);
    }

    /**
     * @dev Transfers tokens from sender to another wallet in the asset manager
     * @param _to the beneficiary wallet
     * @param _amount the amount to be transferred
     * @return bool whether the transfer was successful
     */
    function transfer(address _to, uint256 _amount) public returns (bool success) {
        success = game.transfer(assetId, msg.sender, _to, _amount);
        emit Transfer(msg.sender, _to, _amount);
        return success;
    }

}


/** @title Authorization contract, works like a whitelist of addresses */
contract Authorized is Ownable {

    mapping(address => bool) public authorized;

    modifier onlyAuthorized() {
        require(authorized[msg.sender], "Sender not authorized");
        _;
    }

    event WalletEnabled(address wallet);
    event WalletDisabled(address wallet);

    /**
     * @dev Adds an address to the authorized list
     * @param _wallet the wallet to be added
     */
    function enableWallet(address _wallet) internal onlyOwner {
        authorized[_wallet] = true;
        emit WalletEnabled(_wallet);
    }

    /**
     * @dev Removes an address to the authorized list
     * @param _wallet the wallet to be removed
     */
    function disableWallet(address _wallet) internal onlyOwner {
        authorized[_wallet] = false;
        emit WalletDisabled(_wallet);
    }

}





library SafeMath {
    /**
     * @dev Returns the addition of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `+` operator.
     *
     * Requirements:
     * - Addition cannot overflow.
     */
    function add(uint256 a, uint256 b) internal pure returns (uint256) {
        uint256 c = a + b;
        require(c >= a, "SafeMath: addition overflow");

        return c;
    }

    /**
     * @dev Returns the subtraction of two unsigned integers, reverting on
     * overflow (when the result is negative).
     *
     * Counterpart to Solidity's `-` operator.
     *
     * Requirements:
     * - Subtraction cannot overflow.
     */
    function sub(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b <= a, "SafeMath: subtraction overflow");
        uint256 c = a - b;

        return c;
    }

    /**
     * @dev Returns the multiplication of two unsigned integers, reverting on
     * overflow.
     *
     * Counterpart to Solidity's `*` operator.
     *
     * Requirements:
     * - Multiplication cannot overflow.
     */
    function mul(uint256 a, uint256 b) internal pure returns (uint256) {
        // Gas optimization: this is cheaper than requiring 'a' not being zero, but the
        // benefit is lost if 'b' is also tested.
        // See: https://github.com/OpenZeppelin/openzeppelin-solidity/pull/522
        if (a == 0) {
            return 0;
        }

        uint256 c = a * b;
        require(c / a == b, "SafeMath: multiplication overflow");

        return c;
    }

    /**
     * @dev Returns the integer division of two unsigned integers. Reverts on
     * division by zero. The result is rounded towards zero.
     *
     * Counterpart to Solidity's `/` operator. Note: this function uses a
     * `revert` opcode (which leaves remaining gas untouched) while Solidity
     * uses an invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function div(uint256 a, uint256 b) internal pure returns (uint256) {
        // Solidity only automatically asserts when dividing by 0
        require(b > 0, "SafeMath: division by zero");
        uint256 c = a / b;
        // assert(a == b * c + a % b); // There is no case in which this doesn't hold

        return c;
    }

    /**
     * @dev Returns the remainder of dividing two unsigned integers. (unsigned integer modulo),
     * Reverts when dividing by zero.
     *
     * Counterpart to Solidity's `%` operator. This function uses a `revert`
     * opcode (which leaves remaining gas untouched) while Solidity uses an
     * invalid opcode to revert (consuming all remaining gas).
     *
     * Requirements:
     * - The divisor cannot be zero.
     */
    function mod(uint256 a, uint256 b) internal pure returns (uint256) {
        require(b != 0, "SafeMath: modulo by zero");
        return a % b;
    }
}




library Roles {
    struct Role {
        mapping (address => bool) bearer;
    }

    /**
     * @dev Give an account access to this role.
     */
    function add(Role storage role, address account) internal {
        require(!has(role, account), "Roles: account already has role");
        role.bearer[account] = true;
    }

    /**
     * @dev Remove an account's access to this role.
     */
    function remove(Role storage role, address account) internal {
        require(has(role, account), "Roles: account does not have role");
        role.bearer[account] = false;
    }

    /**
     * @dev Check if an account has this role.
     * @return bool
     */
    function has(Role storage role, address account) internal view returns (bool) {
        require(account != address(0), "Roles: account is the zero address");
        return role.bearer[account];
    }
}

contract PauserRole {
    using Roles for Roles.Role;

    event PauserAdded(address indexed account);
    event PauserRemoved(address indexed account);

    Roles.Role private _pausers;

    constructor () internal {
        _addPauser(msg.sender);
    }

    modifier onlyPauser() {
        require(isPauser(msg.sender), "PauserRole: caller does not have the Pauser role");
        _;
    }

    function isPauser(address account) public view returns (bool) {
        return _pausers.has(account);
    }

    function addPauser(address account) public onlyPauser {
        _addPauser(account);
    }

    function renouncePauser() public {
        _removePauser(msg.sender);
    }

    function _addPauser(address account) internal {
        _pausers.add(account);
        emit PauserAdded(account);
    }

    function _removePauser(address account) internal {
        _pausers.remove(account);
        emit PauserRemoved(account);
    }
}

/**
 * @dev Contract module which allows children to implement an emergency stop
 * mechanism that can be triggered by an authorized account.
 *
 * This module is used through inheritance. It will make available the
 * modifiers `whenNotPaused` and `whenPaused`, which can be applied to
 * the functions of your contract. Note that they will not be pausable by
 * simply including this module, only once the modifiers are put in place.
 */
contract Pausable is PauserRole {
    /**
     * @dev Emitted when the pause is triggered by a pauser (`account`).
     */
    event Paused(address account);

    /**
     * @dev Emitted when the pause is lifted by a pauser (`account`).
     */
    event Unpaused(address account);

    bool private _paused;

    /**
     * @dev Initializes the contract in unpaused state. Assigns the Pauser role
     * to the deployer.
     */
    constructor () internal {
        _paused = false;
    }

    /**
     * @dev Returns true if the contract is paused, and false otherwise.
     */
    function paused() public view returns (bool) {
        return _paused;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is not paused.
     */
    modifier whenNotPaused() {
        require(!_paused, "Pausable: paused");
        _;
    }

    /**
     * @dev Modifier to make a function callable only when the contract is paused.
     */
    modifier whenPaused() {
        require(_paused, "Pausable: not paused");
        _;
    }

    /**
     * @dev Called by a pauser to pause, triggers stopped state.
     */
    function pause() public onlyPauser whenNotPaused {
        _paused = true;
        emit Paused(msg.sender);
    }

    /**
     * @dev Called by a pauser to unpause, returns to normal state.
     */
    function unpause() public onlyPauser whenPaused {
        _paused = false;
        emit Unpaused(msg.sender);
    }
}



/**
 * @title a contract representing a gambling game. To create a new game,
 * the sender must pay the prize in advance and set two periods: the
 * purchase period and the period in which the winner must be registered.
 * Users may purchase tokens while sales are active. All ETH collected
 * is held in the contract until the game owner registers the winner
 * or the register period expires.
 * If the winner is registered, players holding the winner option
 * can exchange their tokens for the prize.
 * To incentivize honest behaviour from game owners, they can only withdraw
 * ETH collected if they register the winner. Otherwise, if the register period
 * is finished, token holders can redeem their tokens, receiving an equal share
 * of the prize (all holders receive a share).
 */
contract Game is Authorized, Pausable {

    using SafeMath for uint256;

    struct Option {
        uint256 id;
        string name;
        string symbol;
        uint8 decimals;
        uint256 totalSupply;
        uint256 price;
        address token;
    }

    // Global game and token properties
    string public gameName;
    uint256 public prizePerToken;
    uint256 public maxSupplyPerToken;
    uint256 public winnerOptionId;
    uint256 public totalAvailablePrize;
    uint256 public totalTokensSold;

    // Game dedlines
    uint256 public purchaseDeadline;
    uint256 public registerWinnerDeadline;

    /**
     * @dev Checks if an option is registered
     * @param _optionId the option id
     */
    modifier optionExists(uint256 _optionId) {
        require(options[_optionId].totalSupply != 0, "Option not found");
        _;
    }

    mapping(uint256 => Option) public options;
    uint256[] private optionsIndex;
    mapping(uint256 => mapping(address => uint256)) public balances;

    /**
     * @dev Emitted when a new option is registered
     */
    event OptionRegistered(uint256 indexed optionId, string name, string symbol, uint8 decimals, uint256 totalSupply, uint256 price, address token);


    /**
     * @dev Emitted when an asset transfer is made from wallet 'from' to wallet 'to'
     */
    event Transfer(uint256 indexed optionId, address indexed from, address indexed to, uint256 value);


    /**
     * @dev Emitted when an investor buys a token
     */
    event Purchase(uint256 indexed optionId, string symbol, uint256 tokens, uint256 value, address buyer);


    /**
     * @dev Emitted when the game owner registeres the winner option
     */
    event WinnerRegistered(uint256 indexed optionId, uint256 availablePrize);


    /**
     * @dev When a token holder withdraws its prize
     */
    event PrizeClaimed(address indexed requester, uint256 totalPrize);


    /**
     * @dev Emitted when a token holder redeems its tokens
     */
    event TokensRedeemed(address indexed requester, uint256 optionId, uint256 tokens, uint256 totalWithdrawn);

    /**
     * @dev The constructor sets general game data and deadlines
     * @param _name the game name
     * @param _prizePerToken the amount in ETH to be paid for each winner token
     * @param _maxSupplyPerToken the max amount of tokens to be created for any option.
        Used to ensure the prize payment will be enough to pay all holders
     * @param _purchaseDeadline the purchase period end. Users cannot buy new tokens after that
     * @param _registerWinnerDeadline the period in which the game owner can register the winner option.
        If winner is not registered until this deadline, users can start redeeming their tokens
     */
    constructor(string memory _name, uint256 _prizePerToken, uint256 _maxSupplyPerToken, uint256 _purchaseDeadline, uint256 _registerWinnerDeadline)
        payable
        public {
            // validations
            require(_prizePerToken > 0, "Prize cannot be zero");
            require(_maxSupplyPerToken > 0, "Max supply per token cannot be zero");
            require(msg.value >= _prizePerToken.mul(_maxSupplyPerToken), "Not enough funds to create a game");
            require(_purchaseDeadline > now, "Invalid purchase deadline");
            require(_registerWinnerDeadline > _purchaseDeadline, "Invalid register winner deadline");
            // effects
            gameName = _name;
            prizePerToken = _prizePerToken;
            maxSupplyPerToken = _maxSupplyPerToken;
            purchaseDeadline = _purchaseDeadline;
            registerWinnerDeadline = _registerWinnerDeadline;
    }

    function() external payable {
        revert("You must call buyTokens(uint256 _optionId) to buy tokens");
    }

    /**
     * @dev Registers an options, which will be represented by a token
     * @param _optionId the unique option id
     * @param _name the option/token name
     * @param _symbol the option/token symbol
     * @param _decimals the option/token decimals
     * @param _totalSupply the option/token totalSupply
     * @param _price the option price. This is the value investors must pay for 1 token
     */
    // TODO: move name, symbol and decimals(?) offchain, replace for a IPFS where to get rich data from (like ERC 1155)
    function registerOption(uint256 _optionId, string memory _name, string memory _symbol, uint8 _decimals, uint256 _totalSupply, uint256 _price)
        public
        onlyOwner {
            // validations
            require(_totalSupply > 0 && _totalSupply <= maxSupplyPerToken, "Invalid total supply");
            require(_price > 0, "Price cannot be zero");
            require(options[_optionId].totalSupply == 0, "Option already registered");
            // effects
            // set the supply to this contract balance so it can be sold
            balances[_optionId][address(this)] = _totalSupply;
            // adds to the index
            optionsIndex.push(_optionId);
            // creates a token emulator so tokens can be checked and transferred in any ERC-20 enabled wallet
            ERC20Emulator emulator = new ERC20Emulator(address(this), _optionId);
            address emulatorAddress = address(emulator);
            // authorize emulator to forward transfer requests to this game
            enableWallet(emulatorAddress);
            // registers option
            options[_optionId] = Option(_optionId, _name, _symbol, _decimals, _totalSupply, _price, emulatorAddress);
            // event
            emit OptionRegistered(_optionId, _name, _symbol, _decimals, _totalSupply, _price, emulatorAddress);
    }

    /**
     * @dev Transfers tokens from '_from' address to '_to' address. Only authorized addresses can call this function
     * @param _optionId the unique option id
     * @param _from the current tokens onwer's address
     * @param _to the beneficiary's address
     * @param _value the amount of tokens to be transferred
     * @return bool whether the transfer was successful
     */
    function transfer(uint256 _optionId, address _from, address _to, uint256 _value)
        external
        optionExists(_optionId)
        onlyAuthorized
        whenNotPaused
        returns (bool) {
            // validations
            require(_to != address(0), "Invalid beneficiary address");
            // effects
            return _transfer(_optionId, _from, _to, _value);
    }

    /**
     * @dev Function to buy tokens. The amount of tokens is calculated from the ETH sent
     * @param _optionId the unique option id
     */
    function buyTokens(uint256 _optionId)
        external
        payable
        optionExists(_optionId) {
            // validations
            require(now < purchaseDeadline, "Purchase period has ended");
            //effects
            Option memory option = options[_optionId];
            // check price and value sent
            require(msg.value >= option.price, "Not enough funds to buy a token");
            // calculates token amount
            uint256 tokenAmount = msg.value.div(option.price);
            // update total sold tokens balance
            totalTokensSold += tokenAmount;
            // transfer tokens to sender
            require(_transfer(_optionId, address(this), msg.sender, tokenAmount), "Transfer error");
            // emit event
            emit Purchase(_optionId, option.symbol, tokenAmount, msg.value, msg.sender);
    }

    /**
     * @dev Registers the winner option. This can only be done after the purchaseDeadline is reached
     * and before the registerWinnerDeadline. By registering the option, the game owner can retrieve
     * all ETH collected by tokens sales plus the prize allocated for unsold tokens
     * @param _optionId the unique option id
     */
    // TODO: Improvements - get result from an oracle
    function registerWinner(uint256 _optionId)
        external
        onlyOwner
        optionExists(_optionId) {
            // validations
            require(winnerOptionId == 0, "Winner has already been set");
            require(now >= purchaseDeadline, "Game is still open to bets");
            require(now < registerWinnerDeadline, "Register winner period has ended");
            // effects
            // sets the winner before transferring (prevents reentracy attacks)
            winnerOptionId = _optionId;
            // gets the winner structure...
            Option storage winner = options[_optionId];
            // ...and calculates the total prize
            totalAvailablePrize = winner.totalSupply
                .sub(balances[_optionId][address(this)]) // gets the amount sold
                .mul(prizePerToken);
            // transfers the total collected plus total prize for tokens unsold to the owner
            msg.sender.transfer(address(this).balance.sub(totalAvailablePrize));
            // logs
            emit WinnerRegistered(_optionId, totalAvailablePrize);
    }

    /**
     * @dev Allows winners to withdraw their prize in ETH. The prize is calculated from
     * the sender's balance and prizePerToken
     */
    function claimPrize()
        external {
            // validations
            require(winnerOptionId != 0, "Winner not set");
            // calculates sender's prize
            uint256 totalPrize = balances[winnerOptionId][msg.sender].mul(prizePerToken);
            // burns sender's tokens before transferring (prevents reentrancy attacks)
            balances[winnerOptionId][msg.sender] = 0;
            // transfers prize to sender
            msg.sender.transfer(totalPrize);
            // logs
            emit PrizeClaimed(msg.sender, totalPrize);
    }

    /**
     * @dev "It's every man for himself" function. If game owner didn't register the winner
     * in time, token holders can take back the ETH spent on purchases.
     */
    function redeemTokens(uint256 _optionId)
        external
        optionExists(_optionId) {
            // validations
            require(winnerOptionId == 0, "Cannot reedeem tokens as winner option is set");
            require(now >= registerWinnerDeadline, "Cannot reedeem tokens before register winner period is finished");
            require(balances[_optionId][msg.sender] > 0, "You don't have any tokens of this type");
            // effects
            Option storage option = options[_optionId];
            // calculates invested ETH plus the share in the prize (equally distributed between all players)
            uint256 tokenBalance = balances[_optionId][msg.sender];
            uint256 amount = option.price.mul(tokenBalance) // total paid for tokens
                .add(
                    prizePerToken.mul(maxSupplyPerToken) // total prize sent on game creation...
                    .div(totalTokensSold) // ...get the proportional prize per token
                    .mul(tokenBalance) // ...multiplied by the amount of tokens owned
                );
            // burns all sender's tokens before transferring
            balances[_optionId][msg.sender] = 0;
            // transfers the amount
            msg.sender.transfer(amount);
            // logs
            emit TokensRedeemed(msg.sender, _optionId, tokenBalance, amount);
    }

    /**
     * @dev Internal transfer function. Transfer tokens from a wallet for another. Reverts if
     * sender's balance is insufficient for the transfer
     * @param _optionId the unique option id
     * @param _from the current tokens onwer's address
     * @param _to the beneficiary's address
     * @param _value the amount of tokens to be transferred
     * @return bool whether the transfer was successful
     */
    function _transfer(uint256 _optionId, address _from, address _to, uint256 _value)
        internal
        returns(bool) {
            // effects
            balances[_optionId][_from] = balances[_optionId][_from].sub(_value);
            balances[_optionId][_to] = balances[_optionId][_to].add(_value);
            // logs
            emit Transfer(_optionId, _from, _to, _value);
            return true;
    }

    /**
     * @dev Gets the name of an asset
     * @param _optionId the asset id
     * @return string the name of the asset
     */
    function name(uint256 _optionId) 
        external
        view
        returns (string memory) {
            return options[_optionId].name;
    }

    /**
     * @dev Gets the symbol of an asset
     * @param _optionId the asset id
     * @return string the symbol of the asset
     */
    function symbol(uint256 _optionId)
        external
        view
        returns (string memory) {
            return options[_optionId].symbol;
    }

    /**
     * @dev Gets the decimals of an asset
     * @param _optionId the asset id
     * @return uint8 the decimals of the asset
     */
    function decimals(uint256 _optionId)
        external
        view
        returns (uint8) {
            return options[_optionId].decimals;
    }

    /**
     * @dev Gets the total supply of an asset
     * @param _optionId the asset id
     * @return uint256 the total supply of the asset
     */
    function totalSupply(uint256 _optionId)
        external
        view
        returns (uint256) {
            return options[_optionId].totalSupply;
    }

    /**
     * @dev Checks the balance of a wallet in the asset manager
     * @param _optionId the asset id
     * @param _owner the wallet to be checked
     * @return uint256 the balance
     */
    function balanceOf(uint256 _optionId, address _owner)
        external
        view
        returns (uint256) {
            return balances[_optionId][_owner];
    }

    /**
     * @dev Gets all registered options
     * @return uint256[] the options array
     */
    function index()
        external
        view
        returns (uint256[] memory) {
            return optionsIndex;
    }


}
