Some common patterns were used in this project:

Withdrawal pattern (pull payments)
The withdrawal pattern was implemented in Game contract to allow users collect their prize. This
is important because actively sending users their prizes could result in funds being permanently
locked in the contract, as iterating through token holders's balances and transferring ETH could
easily hit the block gas limit; or malicious users could buy tokens from a contract which fails
upon transfers, rendering a "transferPrizes" function useless (and funds stuck forever);

Restricting access
Some functions in the Game contract must be restricted to its owner; registering options and the
winner token are restricted because they could be used by users to change the game balance or, most
probably, to set the winner token to the token they own.
Another important restriction is the onlyAuthorized, which allows addresses to move token balances.
It's important that this restriction happens on a different layer than the owners', because owners
mustn't be able to transfer tokens on behalf (or in disfavor) of token holders.

State machine
The Game contract enables or disables features according to three different states: purchase period,
register winner period, winner registered and register winner period expired.
During purchasing period, game owners cannot register winners and users can't claim or redeem tokens;
when register winner period is active, users can't to anything; if winner is registered, users can
claim their prizes; if winner is not registered and the time for that has ended, game owner can't
do anything else but token holders can redeem their tokens.

Fail early
Make all validations before changing state and push transfers to the end of functions. This pattern
is implemented in all functions.