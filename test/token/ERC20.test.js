const {
    BN,
    ether,
    expectEvent,
    expectRevert,
    time
} = require('openzeppelin-test-helpers')
const {
    expect
} = require('chai')

const Game = artifacts.require('Game');
const ERC20Emulator = artifacts.require('ERC20Emulator');

contract('ERC20', function ([gameOwner, recipient, anotherAccount]) {

    let initialHolder, token, game, purchaseDeadline, registerWinnerDeadline
    const initialSupply = new BN(100)
    const gameId = '0xabcdef01234569789'
    const errorPrefix = 'ERC20'

    beforeEach(async () => {
        purchaseDeadline = (await time.latest()).add(time.duration.minutes(5))
        registerWinnerDeadline = (await time.latest()).add(time.duration.minutes(10))
        // creates a new game
        game = await Game.new('Test Game', ether('1'), new BN(100), purchaseDeadline, registerWinnerDeadline, {
            from: gameOwner,
            value: ether('100')
        })
        // registers one option...
        await game.registerOption(gameId, 'name', 'symbol', 0, initialSupply, ether('1'), {
            from: gameOwner
        })
        // ...which will me mapped to a token emulator
        token = await ERC20Emulator.new(game.address, gameId);
        // sets the game contract as initial holder
        initialHolder = game.address
    });

    // shouldBehaveLikeERC20 tests
    describe('total supply', function () {
        it('returns the total amount of tokens', async function () {
            expect(await token.totalSupply()).to.be.bignumber.equal(initialSupply);
        });
    });

    describe('balanceOf', function () {
        describe('when the requested account has no tokens', function () {
            it('returns zero', async function () {
                expect(await token.balanceOf(anotherAccount)).to.be.bignumber.equal('0');
            });
        });

        describe('when the requested account has some tokens', function () {
            it('returns the total amount of tokens', async function () {
                expect(await token.balanceOf(initialHolder)).to.be.bignumber.equal(initialSupply);
            });
        });

    })

    // TODO: test transfers

});