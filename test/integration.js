const expect = require('expect.js')
const { BN, constants, expectEvent, expectRevert, ether, time, balance } = require('openzeppelin-test-helpers')

const Game = artifacts.require('Game.sol')
const Emulator = artifacts.require('ERC20Emulator.sol')

contract('The Game contract', function([ deployer, gameOwner, investor, anotherInvestor, randomGuy ]) {
    
    let game, purchaseDeadline, registerWinnerDeadline, tokenAddress, token
    let investorTokenBalance = new BN(0)
    let contractBalance = new BN(100)
    // general
    const gasPrice = new BN(3)
    const ZERO = new BN(0)
    const ONE = new BN(1)
    const TWO = new BN(2)
    const FIVE = new BN(5)
    // game params
    const gameName = 'Test Game'
    const prizePerToken = ether('1')
    const supplyPerToken = new BN(100)
    const payment = ether('100')
    // option params
    const optionId = new BN(587469)
    const name = 'Brazil'
    const symbol = 'BRA'
    const supply = new BN(100)
    const decimals = ZERO
    const price = ether('0.285')
    
    before(async () => {
        const now = await time.latest()
        purchaseDeadline = now.add(time.duration.minutes(5))
        registerWinnerDeadline = now.add(time.duration.minutes(10))
        game = await Game.new(gameName, prizePerToken, supplyPerToken, purchaseDeadline, registerWinnerDeadline, { from: gameOwner, value: payment })
    })

    const updateBalances = purchased => {
        investorTokenBalance = investorTokenBalance.add(purchased)
        contractBalance = contractBalance.sub(purchased)
    }

    describe.only('simulating a game', () => {

        it('should allow game creator to register an option', async () => {
            const { logs } = await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            tokenAddress = logs[0].args.wallet
            expectEvent.inLogs(logs, 'WalletEnabled', {
                wallet: tokenAddress
            })
            expectEvent.inLogs(logs, 'OptionRegistered', {
                optionId,
                name,
                symbol,
                decimals,
                totalSupply: supply
            })
        })

        it('should create a ERC20 emulator', async () => {
            token = await Emulator.at(tokenAddress)
            expect(await token.symbol()).to.be(symbol)
        })

        it('should allow querying individual option properties by calling getters', async () => {
            expect(await game.name(optionId)).to.eql(name)
            expect(await game.symbol(optionId)).to.eql(symbol)
            expect(await game.decimals(optionId)).to.eql(decimals)
            expect(await game.totalSupply(optionId)).to.eql(supply)
        })

        describe('when purchasing', () => {

            it('should update total sold tokens balance', async () => {
                await game.buyTokens(optionId, { from: investor, value: price.mul(FIVE) })
                updateBalances(FIVE)
                expect(await game.totalTokensSold()).to.be.eql(FIVE)
                expect(await game.balanceOf(optionId, investor)).to.be.eql(FIVE)
            })

            it('should transfer tokens to sender wallet upon a valid purchase', async () => {
                await game.buyTokens(optionId, { from: investor, value: price })
                updateBalances(ONE)
                expect(await game.balanceOf(optionId, investor)).to.be.eql(investorTokenBalance)
                expect(await game.balanceOf(optionId, game.address)).to.be.eql(contractBalance)
            })

            it('should control balances correctly', async () => {
                await game.buyTokens(optionId, { from: anotherInvestor, value: price })
                contractBalance = contractBalance.sub(ONE)
                expect(await game.balanceOf(optionId, anotherInvestor)).to.be.eql(ONE)
                expect(await game.balanceOf(optionId, game.address)).to.be.eql(contractBalance)
            })

            it('should allow subsequent purchases', async () => {
                await game.buyTokens(optionId, { from: anotherInvestor, value: price })
                contractBalance = contractBalance.sub(ONE)
                expect(await game.balanceOf(optionId, anotherInvestor)).to.be.eql(TWO)
                expect(await game.balanceOf(optionId, game.address)).to.be.eql(contractBalance)
            })

        })

        describe('when transferring tokens', () => {

            it('should correctly update balances', async () => {
                await token.transfer(randomGuy, ONE, { from: anotherInvestor })
                expect(await game.balanceOf(optionId, anotherInvestor)).to.be.eql(ONE)
                expect(await game.balanceOf(optionId, randomGuy)).to.be.eql(ONE)
            })

        })

        describe('when winner token is registered', () => {

            it('should send collected ETH and unsold token prizes to owner', async () => {
                const tracker = await balance.tracker(gameOwner)
                await time.increaseTo(purchaseDeadline)
                const { receipt: { gasUsed } } = await game.registerWinner(optionId, { from: gameOwner, gasPrice })
                const gasPaid = new BN(gasUsed).mul(gasPrice)
                const expectedContractBalance = investorTokenBalance.add(TWO).mul(prizePerToken)
                expect(await balance.current(game.address)).to.eql(expectedContractBalance)
                const expectedETHreceived = new BN(92).mul(prizePerToken) // unsold tokens
                    .add(price.mul(investorTokenBalance.add(TWO))) // sold tokens
                    .sub(gasPaid) // gas used in transaction
                    .toString()
                expect((await tracker.delta()).toString()).to.eql(expectedETHreceived)
            })

            it('should pay the prize to all token holders', async () => {

                const testIt = async wallet => {
                    const tracker = await balance.tracker(wallet)
                    const tokenBalance = await token.balanceOf(wallet)
                    const { receipt: { gasUsed } } = await game.claimPrize({ from: wallet, gasPrice })
                    const gasPaid = new BN(gasUsed).mul(gasPrice)
                    expect((await tracker.delta()).toString()).to.eql(tokenBalance.mul(prizePerToken).sub(gasPaid).toString())
                    expect(await token.balanceOf(wallet)).to.eql(ZERO)
                }

                await testIt(investor)
                await testIt(anotherInvestor)
                await testIt(randomGuy)
            })

        })

        describe('when winner token is not registered in time', () => {

            const optionId2 = 111

            before(async () => {
                const now = await time.latest()
                purchaseDeadline = now.add(time.duration.minutes(5))
                registerWinnerDeadline = now.add(time.duration.minutes(10))
                game = await Game.new(gameName, prizePerToken, supplyPerToken, purchaseDeadline, registerWinnerDeadline, { from: gameOwner, value: payment })
                await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
                await game.registerOption(optionId2, name, symbol, decimals, supply, TWO.mul(price), { from: gameOwner })
                await game.buyTokens(optionId, { from: investor, value: price })
                await game.buyTokens(optionId2, { from: anotherInvestor, value: TWO.mul(price) })
                await time.increaseTo(registerWinnerDeadline)
            })

            it('should allow token holders to redeem their tokens', async () => {
                const testIt = wallet => optionId => async price => {
                    const tracker = await balance.tracker(wallet)
                    const { receipt: { gasUsed } } = await game.redeemTokens(optionId, { from: wallet, gasPrice })
                    const gasPaid = new BN(gasUsed).mul(gasPrice)
                    const redeemAmount = supply
                        .mul(prizePerToken) // total prize
                        .div(await game.totalTokensSold()) // proportional prize
                        .mul(ONE) // tokens bought
                        .add(price) // paid for the token
                    expect((await tracker.delta()).toString()).to.be(redeemAmount.sub(gasPaid).toString())
                }

                await testIt(investor)(optionId)(price)
                await testIt(anotherInvestor)(optionId2)(TWO.mul(price))

                expect(await balance.current(game.address)).to.eql(ZERO)
            })

        })

    })

})
