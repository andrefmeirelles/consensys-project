const expect = require('expect.js')
const { BN, constants, expectEvent, expectRevert, ether, time, balance } = require('openzeppelin-test-helpers')
const { ZERO_ADDRESS } = constants
const moment = require('moment')

const Game = artifacts.require('Game.sol')
const Wrapper = artifacts.require('ERC20Emulator.sol')

contract('The Game contract', function([ deployer, gameOwner, investor, randomGuy ]) {

    let game, purchaseDeadline, registerWinnerDeadline
    // game params
    const gameName = 'Test Game'
    const prizePerToken = ether('1')
    const supplyPerToken = new BN(100)
    const payment = ether('100')
    // option params
    const optionId = new BN(587469)
    const name = 'World Cup'
    const symbol = 'WCup'
    const supply = new BN(100)
    const decimals = new BN(0)
    const price = ether('0.5')
    // general
    const gasPrice = new BN(3)
    
    beforeEach(async () => {
        purchaseDeadline = (await time.latest()).add(time.duration.minutes(5))
        registerWinnerDeadline = (await time.latest()).add(time.duration.minutes(10))
        game = await Game.new(gameName, prizePerToken, supplyPerToken, purchaseDeadline, registerWinnerDeadline, { from: gameOwner, value: payment })
    })

    describe('upon creation', () => {

        it('should revert if prize per token is 0', async () => {
            await expectRevert(Game.new(gameName, 0, supplyPerToken, purchaseDeadline, registerWinnerDeadline, { from: gameOwner }), 'Prize cannot be zero')
        })

        it('should revert if max supply per token is 0', async () => {
            await expectRevert(Game.new(gameName, prizePerToken, 0, purchaseDeadline, registerWinnerDeadline, { from: gameOwner, value: payment }), 'Max supply per token cannot be zero')
        })

        it('should revert if not enough ether is sent', async () => {
            await expectRevert(Game.new(gameName, prizePerToken, supplyPerToken, purchaseDeadline, registerWinnerDeadline, { from: gameOwner }), 'Not enough funds to create a game')
        })

        it('should revert purchase deadline is before now', async () => {
            await expectRevert(Game.new(gameName, prizePerToken, supplyPerToken, moment().subtract(2, 'seconds').format('X'), registerWinnerDeadline, { from: gameOwner, value: payment }), 'Invalid purchase deadline')
        })

        it('should revert if register deadline is before purchase deadline', async () => {
            await expectRevert(Game.new(gameName, prizePerToken, supplyPerToken, purchaseDeadline, moment().format('X'), { from: gameOwner, value: payment }), 'Invalid register winner deadline')
        })

        it('should correctly set up properties', async () => {
            expect(await game.gameName()).to.be(gameName)
            expect(await game.prizePerToken()).to.eql(prizePerToken)
            expect(await game.maxSupplyPerToken()).to.eql(supplyPerToken)
            expect(await game.purchaseDeadline()).to.eql(purchaseDeadline)
            expect(await game.registerWinnerDeadline()).to.eql(registerWinnerDeadline)
        })

    })

    describe('when registering an option', () => {

        it('should revert if sender is not the owner', async () => {
            await expectRevert(game.registerOption(optionId, name, symbol, decimals, supply, price, { from: investor }), 'Ownable: caller is not the owner')
        })

        it('should revert if total supply is 0', async () => {
            await expectRevert(game.registerOption(optionId, name, symbol, decimals, 0, price, { from: gameOwner }), 'Invalid total supply')
        })

        it('should revert if total supply exceeds max supply per token', async () => {
            await expectRevert(game.registerOption(optionId, name, symbol, decimals, 101, price, { from: gameOwner }), 'Invalid total supply')
        })

        it('should register the option with correct properties', async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            const option = await game.options(optionId)
            expect(option.id).to.eql(optionId)
            expect(option.name).to.eql(name)
            expect(option.symbol).to.eql(symbol)
            expect(option.decimals).to.eql(decimals)
            expect(option.totalSupply).to.eql(supply)
        })

        it('should be given to the contract the total supply of tokens', async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            expect(await game.balanceOf(optionId, game.address)).to.eql(supply)
        })

        it('should emit an option registered event', async () => {
            const { logs } = await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            expectEvent.inLogs(logs, 'OptionRegistered', {
                optionId,
                name,
                symbol,
                decimals,
                totalSupply: supply
            })
            expect(logs[1].args.token).not.to.be('0x0')
        })

        it('should allow querying individual option properties by calling getters', async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            expect(await game.name(optionId)).to.eql(name)
            expect(await game.symbol(optionId)).to.eql(symbol)
            expect(await game.decimals(optionId)).to.eql(decimals)
            expect(await game.totalSupply(optionId)).to.eql(supply)
        })

        it('should create a token wrapper and authorized it', async () => {
            const { logs } = await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            const wrapper = await Wrapper.at(logs[1].args.token)
            expect(await wrapper.balanceOf(game.address)).to.eql(supply)
        })

    })

    describe('when transferring tokens', () => {

        let tokenAddress, contractAddress
        const transferAmount = new BN(10)

        beforeEach(async () => {
            const { logs } = await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            tokenAddress = logs[0].args.wallet
            contractAddress = game.address
        })

        it('should revert if option is not found', async () => {
            await expectRevert(game.transfer(111, contractAddress, investor, transferAmount, { from: tokenAddress }), 'Option not found')
        })

        it('should revert if beneficiary address is empty', async () => {
            await expectRevert(game.transfer(optionId, contractAddress, ZERO_ADDRESS, transferAmount, { from: tokenAddress }), 'Invalid beneficiary address')
        })

        it('should revert if sender does not have enough funds', async () => {
            await expectRevert(game.transfer(optionId, contractAddress, investor, 110, { from: tokenAddress }), 'SafeMath: subtraction overflow.')
        })

        // TODO: test transfers

    })

    describe('when selling tokens', () => {

        beforeEach(async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
        })

        it('should revert if option does not exist', async () => {
            await expectRevert(game.buyTokens(111, { from: investor, value: price }), 'Option not found')
        })

        it('should revert if period time has ended', async () => {
            await time.increaseTo(purchaseDeadline)
            await expectRevert(game.buyTokens(optionId, { from: investor, value: price }), 'Purchase period has ended')
        })

        it('should revert if ETH sent is lesser than the token price', async () => {
            await expectRevert(game.buyTokens(optionId, { from: investor, value: ether('0.499') }), 'Not enough funds to buy a token')
        })

        it('should revert if ETH sent exceeds the current token supply', async () => {
            await expectRevert(game.buyTokens(optionId, { from: investor, value: ether('101') }), 'SafeMath: subtraction overflow')
        })

        it('should update total sold tokens balance', async () => {
            const four = new BN(4)
            await game.buyTokens(optionId, { from: investor, value: price.mul(four) })
            expect(await game.totalTokensSold()).to.be.eql(four)
        })

        it('should transfer tokens to sender wallet upon a valid purchase', async () => {
            await game.buyTokens(optionId, { from: investor, value: price })
            expect(await game.balanceOf(optionId, investor)).to.be.eql(new BN(1))
            expect(await game.balanceOf(optionId, game.address)).to.be.eql(new BN(99))
        })

        it('should round down ETH/price divisions', async () => {
            await game.buyTokens(optionId, { from: investor, value: ether('0.999') })
            expect(await game.balanceOf(optionId, investor)).to.be.eql(new BN(1))
            expect(await game.balanceOf(optionId, game.address)).to.be.eql(new BN(99))
        })

        it('should emit purchase and transfer events', async () => {
            const { logs } = await game.buyTokens(optionId, { from: investor, value: price })
            expectEvent.inLogs(logs, 'Purchase', {
                optionId,
                symbol,
                tokens: new BN(1),
                value: price,
                buyer: investor
            })
            expectEvent.inLogs(logs, 'Transfer', {
                optionId,
                from: game.address,
                to: investor,
                value: new BN(1)
            })
        })

    })

    describe('when registering the winner', () => {

        beforeEach(async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
        })

        it('should revert if sender is not the owner', async () => {
            await expectRevert(game.registerWinner(optionId, { from: investor }), 'Ownable: caller is not the owner')
        })

        it('should revert if option does not exist', async () => {
            await expectRevert(game.registerWinner(111, { from: gameOwner }), 'Option not found')
        })

        it('should revert if winner has been set already', async () => {
            await time.increaseTo(purchaseDeadline)
            await game.registerWinner(optionId, { from: gameOwner })
            await expectRevert(game.registerWinner(optionId, { from: gameOwner }), 'Winner has already been set')
        })

        it('should revert if purchase period is still active', async () => {
            await expectRevert(game.registerWinner(optionId, { from: gameOwner }), 'Game is still open to bets')
        })

        it('should revert if register winner period is finished', async () => {
            await time.increaseTo(registerWinnerDeadline + 1)
            await expectRevert(game.registerWinner(optionId, { from: gameOwner }), 'Register winner period has ended')
        })

        it('should set the winner in the contract', async () => {
            await time.increaseTo(purchaseDeadline)
            await game.registerWinner(optionId, { from: gameOwner })
            expect(await game.winnerOptionId()).to.eql(optionId)
        })

        it('should set the correct available prize in the contract', async () => {
            await game.buyTokens(optionId, { from: investor, value: price })
            await time.increaseTo(purchaseDeadline)
            await game.registerWinner(optionId, { from: gameOwner })
            expect(await game.totalAvailablePrize()).to.eql(prizePerToken)
        })

        it('should save the prize ETH and send the remainder to owner', async () => {
            const tracker = await balance.tracker(gameOwner)
            const four = new BN(4)
            await game.buyTokens(optionId, { from: investor, value: price.mul(four) })
            await time.increaseTo(purchaseDeadline)
            const { receipt: { gasUsed } } = await game.registerWinner(optionId, { from: gameOwner, gasPrice })
            const gasPaid = new BN(gasUsed).mul(gasPrice)
            expect(await balance.current(game.address)).to.eql(prizePerToken.mul(four))
            expect((await tracker.delta()).toString()).to.eql(
                supply.sub(four).mul(prizePerToken) // unsold tokens
                .add(price.mul(four)) // sold tokens
                .sub(gasPaid) // gas used in transaction
                .toString()
            )
        })

        it('should emit a winner registered event', async () => {
            await game.buyTokens(optionId, { from: investor, value: price })
            await time.increaseTo(purchaseDeadline)
            const { logs } = await game.registerWinner(optionId, { from: gameOwner })
            expectEvent.inLogs(logs, 'WinnerRegistered',{
                optionId,
                availablePrize: prizePerToken
            })
        })

    })

    describe('when claiming prize', () => {

        beforeEach(async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            await game.buyTokens(optionId, { from: investor, value: price })
            await time.increaseTo(purchaseDeadline)
        })

        it('should revert if winner option is not set', async () => {
            await expectRevert(game.claimPrize({ from: investor }), 'Winner not set')
        })

        it('should burn requester tokens and transfer the prize', async () => {
            await game.registerWinner(optionId, { from: gameOwner })
            const contractTracker = await balance.tracker(game.address)
            const investorTracker = await balance.tracker(investor)
            const { receipt: { gasUsed } } = await game.claimPrize({ from: investor, gasPrice })
            const gasPaid = new BN(gasUsed).mul(gasPrice)
            expect(await contractTracker.delta()).to.eql(prizePerToken.neg())
            expect((await investorTracker.delta()).toString()).to.be(prizePerToken.sub(gasPaid).toString())
        })

        it('should emit a prize claimed event', async () => {
            await game.registerWinner(optionId, { from: gameOwner })
            const { logs } = await game.claimPrize({ from: investor })
            expectEvent.inLogs(logs, 'PrizeClaimed', {
                requester: investor,
                totalPrize: prizePerToken
            })
        })

    })

    describe('when redeeming tokens', () => {

        beforeEach(async () => {
            await game.registerOption(optionId, name, symbol, decimals, supply, price, { from: gameOwner })
            await game.buyTokens(optionId, { from: investor, value: price })
        })
        
        it('should revert if option does not exist', async () => {
            await expectRevert(game.redeemTokens(111, { from: investor }), 'Option not found')
        })
        
        it('should revert register winner period is still active', async () => {
            await expectRevert(game.redeemTokens(optionId, { from: investor }), 'Cannot reedeem tokens before register winner period is finished')
        })

        it('should revert if sender did not buy any tokens', async () => {
            await time.increaseTo(registerWinnerDeadline)
            await expectRevert(game.redeemTokens(optionId, { from: randomGuy }), "You don't have any tokens of this type")
        })

        it('should burn all sender tokens and transfer the correct amount of ETH to their wallet', async () => {
            await time.increaseTo(registerWinnerDeadline)
            const contractTracker = await balance.tracker(game.address)
            const investorTracker = await balance.tracker(investor)
            const { receipt: { gasUsed } } = await game.redeemTokens(optionId, { from: investor, gasPrice })
            const gasPaid = new BN(gasUsed).mul(gasPrice)
            const redeemAmount = supply
                .mul(prizePerToken) // total prize
                .div(new BN(1)) // only 1 token bought
                .add(price) // paid for 1 token
            expect(await contractTracker.delta()).to.eql(redeemAmount.neg())
            expect((await investorTracker.delta()).toString()).to.be(redeemAmount.sub(gasPaid).toString())
        })

        it('should emit a tokens redeemed event', async () => {
            await time.increaseTo(registerWinnerDeadline)
            const { logs } = await game.redeemTokens(optionId, { from: investor, gasPrice })
            const redeemAmount = supply
                .mul(prizePerToken) // total prize
                .div(new BN(1)) // only 1 token bought
                .add(price) // paid for 1 token
            expectEvent.inLogs(logs, 'TokensRedeemed', {
                requester: investor,
                optionId,
                tokens: new BN(1),
                totalWithdrawn: redeemAmount
            })
        })

    })

})
