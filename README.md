# Betting Game dApp

This dApp was built as the final project for Consensys' Developer On Demand course. It is a simple gambling game for waging in countries participating in a World Cup. From the UI, it looks and feels like the user is purchasing tokens, but under the hood all token balances are managed by a single contract,
which reduces the amount of gas required to create new betting options, as each option creates a connector made to emulate an ERC-20 for wallets.

The main idea behind the smart contracts design is to allow people to create their own games, for users to bet on options and, after the sales period is finished, to allow the creation of a secondary market. This enables users to buy and sell tokens according to changes in the odds (in this case,
the performance of teams).

Games have three different stages: a period in which tokens can be bought, another one when game owners can register the winner token and the token payment phase.

In order to create a new game, the total prize must be paid in advance. The total prize is calculated from the maximum supply allowed for options multiplied by the prize per token. This amount is locked up in the contract until one of the following happens:

- The game owner registers the winner token
- The period to register the winner token expires

In case the game owner registers the winner, than all ETH collected by token sales AND the prize relative to unsold tokens are transferred to the game owner. Users are then allowed to withdraw their prizes.

In the scenario where game owners don't register the winner in time, they lose not only the ETH collected, but also the prize paid in advance. Users then can take back their money and total prize is distribuited to all token holders, proportionally to the amount of tokens they redeem.

## How to run this project

1. Clone it
2. Run contracts locally
    - in the project root, ```npm install```
    - test contracts: ```npm test```
    - run: ```npm run local-chain```. This will start ganache-cli.
    - in another terminal tab, run ```truffle deploy```. This will deploy contracts to ganache-cli, but it'll take some time
4. Start client
    - in ```client``` folder, ```npm install```
    - type ```npm start```. This will start the website. Make sure you have metamask installed. Change the network to Localhost 8545.

Make sure you don't change the localhost port to run the application, since the ETH price service has cross-origin requests allowed for localhost:3000 only.

Ganache-cli will start with 10 accounts from a single wallet, with lots of ETH each. Blocks will be sealed each 5s.
Restore wallet from this seed: ```veteran cube come urge fold crack mouse kitchen civil service dry mad```

You can change phase duration in ```migrations/2_deploy_contracts.js```, by setting the variables:

```
const purchasePeriodDurationInMinutes = 10
const registerWinnerPeriodDurationInMinutes = 10
```

You can also change other game properties, like name, prize per token, max supply per token and prize payment in that file.

Country tokens configs are set in ```migrations/country_tokens.js```. You can change invididual token properties in there.

## Next steps:
    Smart contracts:
        - create a controller contract to manage game creation and games index. This contract will be upgradeable through a zos deploy.
        - extract token metadata (name, symbol, decimals etc) to IPFS
    UI:
        - add an opening section where user can choose a profile - game creator or player
        - create a dashboard where game owners can check all their games, create new games, set options etc
        - show a list of all games to players
        - allow users to customize game page by saving images and props in IPFS