const Game = artifacts.require('Game')
const options = require('./country_tokens')
const moment = require('moment')

const purchasePeriodDurationInMinutes = 30
const registerWinnerPeriodDurationInMinutes = 10

module.exports = async function(deployer, network, [ owner ]) {
    const purchaseEnd = moment().add(purchasePeriodDurationInMinutes, 'minutes')
    const registerWinnerEnd = purchaseEnd.clone().add(registerWinnerPeriodDurationInMinutes, 'minutes')
    const name = 'World Cup 2022'
    const prizePerToken = web3.utils.toWei('1', 'ether')
    const maxSupplyPerToken = '100'
    const payment = web3.utils.toWei('100', 'ether')
    
    await deployer.deploy(Game, name, prizePerToken, maxSupplyPerToken, purchaseEnd.format('X'), registerWinnerEnd.format('X'), { value: payment })
    console.log('Registering options, go grab a coffee...')
    const instance = await Game.deployed()
    await Promise.all(
        options.map(async ({ id, name, symbol, decimals, supply, price }) =>
            await instance.registerOption(id, name, symbol, decimals, supply, web3.utils.toWei(price, 'ether'))
        )
    )
};
