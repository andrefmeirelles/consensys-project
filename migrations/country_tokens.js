const options = [
    {
        name: 'Argentina',
        symbol: 'ARG',
        price: '0.1111',
    },
    {
        name: 'Austrialia',
        symbol: 'AUS',
        price: '0.0033',
    },
    {
        name: 'Belgica',
        symbol: 'BEL',
        price: '0.0909',
    },
    {
        name: 'Brazil',
        symbol: 'BRA',
        price: '0.2850',
    },
    {
        name: 'Colombia',
        symbol: 'COL',
        price: '0.0250',
    },
    {
        name: 'Costa Rica',
        symbol: 'CRI',
        price: '0.0020',
    },
    {
        name: 'Croatia',
        symbol: 'HRV',
        price: '0.0303',
    },
    {
        name: 'Denmark',
        symbol: 'DNK',
        price: '0.0100',
    },
    {
        name: 'Egypt',
        symbol: 'EGY',
        price: '0.0067',
    },
    {
        name: 'England',
        symbol: 'ENG',
        price: '0.0556',
    },
    {
        name: 'France',
        symbol: 'FRA',
        price: '0.1538',
    },
    {
        name: 'Germany',
        symbol: 'DEU',
        price: '0.2222',
    },
    {
        name: 'Iceland',
        symbol: 'ISL',
        price: '0.0050',
    },
    {
        name: 'Iran',
        symbol: 'IRN',
        price: '0.0020',
    },
    {
        name: 'Japan',
        symbol: 'JPN',
        price: '0.0033',
    },
    {
        name: 'Mexico',
        symbol: 'MEX',
        price: '0.0100',
    },
    {
        name: 'Morocco',
        symbol: 'MAR',
        price: '0.0020',
    },
    {
        name: 'Nigeria',
        symbol: 'NGA',
        price: '0.0050',
    },
    {
        name: 'Panama',
        symbol: 'PAN',
        price: '0.0010',
    },
    {
        name: 'Peru',
        symbol: 'PER',
        price: '0.0050',
    },
    {
        name: 'Poland',
        symbol: 'POL',
        price: '0.0200',
    },
    {
        name: 'Portugal',
        symbol: 'PRT',
        price: '0.0400',
    },
    {
        name: 'Russia',
        symbol: 'RUS',
        price: '0.0250',
    },
    {
        name: 'Saudi Arabia',
        symbol: 'SAU',
        price: '0.0010',
    },
    {
        name: 'Senegal',
        symbol: 'SEN',
        price: '0.0050',
    },
    {
        name: 'Serbia',
        symbol: 'SRB',
        price: '0.0050',
    },
    {
        name: 'South Korea',
        symbol: 'KOR',
        price: '0.0013',
    },
    {
        name: 'Spain',
        symbol: 'ESP',
        price: '0.1667',
    },
    {
        name: 'Sweden',
        symbol: 'SWE',
        price: '0.0067',
    },
    {
        name: 'Switzerland',
        symbol: 'CHE',
        price: '0.0100',
    },
    {
        name: 'Tunisia',
        symbol: 'TUN',
        price: '0.0013',
    },
    {
        name: 'Uruguay',
        symbol: 'URY',
        price: '0.0303',
    },
]

module.exports = options.map((opt, index) => ({
    ...opt,
    id: index + 1,
    decimals: 0,
    supply: 100,
}))