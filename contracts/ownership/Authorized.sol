pragma solidity ^0.5.8;

import 'openzeppelin-solidity/contracts/ownership/Ownable.sol';

/** @title Authorization contract, works like a whitelist of addresses */
contract Authorized is Ownable {

    mapping(address => bool) public authorized;

    modifier onlyAuthorized() {
        require(authorized[msg.sender], "Sender not authorized");
        _;
    }

    event WalletEnabled(address wallet);
    event WalletDisabled(address wallet);

    /**
     * @dev Adds an address to the authorized list
     * @param _wallet the wallet to be added
     */
    function enableWallet(address _wallet) internal onlyOwner {
        authorized[_wallet] = true;
        emit WalletEnabled(_wallet);
    }

    /**
     * @dev Removes an address to the authorized list
     * @param _wallet the wallet to be removed
     */
    function disableWallet(address _wallet) internal onlyOwner {
        authorized[_wallet] = false;
        emit WalletDisabled(_wallet);
    }

}
