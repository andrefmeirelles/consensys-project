pragma solidity ^0.5.8;

import '../Game.sol';

/**
 * @title An ERC-20 emulator, allowing ERC-20 enabled wallets to interact with
 * an asset manager contract.
 */
contract ERC20Emulator {

    Game public game;
    uint256 public assetId;

    /**
     * @dev Emitted when `value` tokens are moved from one account (`from`) to
     * another (`to`).
     *
     * Note that `value` may be zero.
     */
    event Transfer(address indexed from, address indexed to, uint256 value);

    /**
     * @dev The constructor sets the game and assetId, which cannot be changed
     * @param _game the asset manager contract (in this project, a game)
     * @param _assetId the asset to be wrapped by this contract
     */
    constructor(address payable _game, uint256 _assetId) public {
        game = Game(_game);
        assetId = _assetId;
    }

    /**
     * @dev Emulates the ERC-20 name property
     * @return string the asset name
     */
    function name() public view returns (string memory) {
        return game.name(assetId);
    }

    /**
     * @dev Emulates the ERC-20 symbol property
     * @return string the asset symbol
     */
    function symbol() public view returns (string memory) {
        return game.symbol(assetId);
    }

    /**
     * @dev Emulates the ERC-20 decimals property
     * @return uint8 the asset decimals
     */
    function decimals() public view returns (uint8) {
        return game.decimals(assetId);
    }

    /**
     * @dev Emulates the ERC-20 totalSupply property
     * @return uint256 the asset totalSupply
     */
    function totalSupply() public view returns (uint256) {
        return game.totalSupply(assetId);
    }

    /**
     * @dev Gets the balance of a wallet in the asset manager
     * @param _owner the wallet to be checked
     * @return uint256 the balance
     */
    function balanceOf(address _owner) public view returns (uint256 balance) {
        return game.balanceOf(assetId, _owner);
    }

    /**
     * @dev Transfers tokens from sender to another wallet in the asset manager
     * @param _to the beneficiary wallet
     * @param _amount the amount to be transferred
     * @return bool whether the transfer was successful
     */
    function transfer(address _to, uint256 _amount) public returns (bool success) {
        success = game.transfer(assetId, msg.sender, _to, _amount);
        emit Transfer(msg.sender, _to, _amount);
        return success;
    }

}
