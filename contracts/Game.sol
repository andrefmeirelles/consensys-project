pragma solidity ^0.5.8;

import 'openzeppelin-solidity/contracts/math/SafeMath.sol';
import 'openzeppelin-solidity/contracts/lifecycle/Pausable.sol';
import './ownership/Authorized.sol';
import './token/ERC20Emulator.sol';

/**
 * @title a contract representing a gambling game. To create a new game,
 * the sender must pay the prize in advance and set two periods: the
 * purchase period and the period in which the winner must be registered.
 * Users may purchase tokens while sales are active. All ETH collected
 * is held in the contract until the game owner registers the winner
 * or the register period expires.
 * If the winner is registered, players holding the winner option
 * can exchange their tokens for the prize.
 * To incentivize honest behaviour from game owners, they can only withdraw
 * ETH collected if they register the winner. Otherwise, if the register period
 * is finished, token holders can redeem their tokens, receiving an equal share
 * of the prize (all holders receive a share).
 */
contract Game is Authorized, Pausable {

    using SafeMath for uint256;

    struct Option {
        uint256 id;
        string name;
        string symbol;
        uint8 decimals;
        uint256 totalSupply;
        uint256 price;
        address token;
    }

    // Global game and token properties
    string public gameName;
    uint256 public prizePerToken;
    uint256 public maxSupplyPerToken;
    uint256 public winnerOptionId;
    uint256 public totalAvailablePrize;
    uint256 public totalTokensSold;

    // Game dedlines
    uint256 public purchaseDeadline;
    uint256 public registerWinnerDeadline;

    /**
     * @dev Checks if an option is registered
     * @param _optionId the option id
     */
    modifier optionExists(uint256 _optionId) {
        require(options[_optionId].totalSupply != 0, "Option not found");
        _;
    }

    mapping(uint256 => Option) public options;
    uint256[] private optionsIndex;
    mapping(uint256 => mapping(address => uint256)) public balances;

    /**
     * @dev Emitted when a new option is registered
     */
    event OptionRegistered(uint256 indexed optionId, string name, string symbol, uint8 decimals, uint256 totalSupply, uint256 price, address token);


    /**
     * @dev Emitted when an asset transfer is made from wallet 'from' to wallet 'to'
     */
    event Transfer(uint256 indexed optionId, address indexed from, address indexed to, uint256 value);


    /**
     * @dev Emitted when an investor buys a token
     */
    event Purchase(uint256 indexed optionId, string symbol, uint256 tokens, uint256 value, address buyer);


    /**
     * @dev Emitted when the game owner registeres the winner option
     */
    event WinnerRegistered(uint256 indexed optionId, uint256 availablePrize);


    /**
     * @dev When a token holder withdraws its prize
     */
    event PrizeClaimed(address indexed requester, uint256 totalPrize);


    /**
     * @dev Emitted when a token holder redeems its tokens
     */
    event TokensRedeemed(address indexed requester, uint256 optionId, uint256 tokens, uint256 totalWithdrawn);

    /**
     * @dev The constructor sets general game data and deadlines
     * @param _name the game name
     * @param _prizePerToken the amount in ETH to be paid for each winner token
     * @param _maxSupplyPerToken the max amount of tokens to be created for any option.
        Used to ensure the prize payment will be enough to pay all holders
     * @param _purchaseDeadline the purchase period end. Users cannot buy new tokens after that
     * @param _registerWinnerDeadline the period in which the game owner can register the winner option.
        If winner is not registered until this deadline, users can start redeeming their tokens
     */
    constructor(string memory _name, uint256 _prizePerToken, uint256 _maxSupplyPerToken, uint256 _purchaseDeadline, uint256 _registerWinnerDeadline)
        payable
        public {
            // validations
            require(_prizePerToken > 0, "Prize cannot be zero");
            require(_maxSupplyPerToken > 0, "Max supply per token cannot be zero");
            require(msg.value >= _prizePerToken.mul(_maxSupplyPerToken), "Not enough funds to create a game");
            require(_purchaseDeadline > now, "Invalid purchase deadline");
            require(_registerWinnerDeadline > _purchaseDeadline, "Invalid register winner deadline");
            // effects
            gameName = _name;
            prizePerToken = _prizePerToken;
            maxSupplyPerToken = _maxSupplyPerToken;
            purchaseDeadline = _purchaseDeadline;
            registerWinnerDeadline = _registerWinnerDeadline;
    }

    function() external payable {
        revert("You must call buyTokens(uint256 _optionId) to buy tokens");
    }

    /**
     * @dev Registers an options, which will be represented by a token
     * @param _optionId the unique option id
     * @param _name the option/token name
     * @param _symbol the option/token symbol
     * @param _decimals the option/token decimals
     * @param _totalSupply the option/token totalSupply
     * @param _price the option price. This is the value investors must pay for 1 token
     */
    // TODO: move name, symbol and decimals(?) offchain, replace for a IPFS where to get rich data from (like ERC 1155)
    function registerOption(uint256 _optionId, string memory _name, string memory _symbol, uint8 _decimals, uint256 _totalSupply, uint256 _price)
        public
        onlyOwner {
            // validations
            require(_totalSupply > 0 && _totalSupply <= maxSupplyPerToken, "Invalid total supply");
            require(_price > 0, "Price cannot be zero");
            require(options[_optionId].totalSupply == 0, "Option already registered");
            // effects
            // set the supply to this contract balance so it can be sold
            balances[_optionId][address(this)] = _totalSupply;
            // adds to the index
            optionsIndex.push(_optionId);
            // creates a token emulator so tokens can be checked and transferred in any ERC-20 enabled wallet
            ERC20Emulator emulator = new ERC20Emulator(address(this), _optionId);
            address emulatorAddress = address(emulator);
            // authorize emulator to forward transfer requests to this game
            enableWallet(emulatorAddress);
            // registers option
            options[_optionId] = Option(_optionId, _name, _symbol, _decimals, _totalSupply, _price, emulatorAddress);
            // event
            emit OptionRegistered(_optionId, _name, _symbol, _decimals, _totalSupply, _price, emulatorAddress);
    }

    /**
     * @dev Transfers tokens from '_from' address to '_to' address. Only authorized addresses can call this function
     * @param _optionId the unique option id
     * @param _from the current tokens onwer's address
     * @param _to the beneficiary's address
     * @param _value the amount of tokens to be transferred
     * @return bool whether the transfer was successful
     */
    function transfer(uint256 _optionId, address _from, address _to, uint256 _value)
        external
        optionExists(_optionId)
        onlyAuthorized
        whenNotPaused
        returns (bool) {
            // validations
            require(_to != address(0), "Invalid beneficiary address");
            // effects
            return _transfer(_optionId, _from, _to, _value);
    }

    /**
     * @dev Function to buy tokens. The amount of tokens is calculated from the ETH sent
     * @param _optionId the unique option id
     */
    function buyTokens(uint256 _optionId)
        external
        payable
        optionExists(_optionId) {
            // validations
            require(now < purchaseDeadline, "Purchase period has ended");
            //effects
            Option memory option = options[_optionId];
            // check price and value sent
            require(msg.value >= option.price, "Not enough funds to buy a token");
            // calculates token amount
            uint256 tokenAmount = msg.value.div(option.price);
            // update total sold tokens balance
            totalTokensSold += tokenAmount;
            // transfer tokens to sender
            require(_transfer(_optionId, address(this), msg.sender, tokenAmount), "Transfer error");
            // emit event
            emit Purchase(_optionId, option.symbol, tokenAmount, msg.value, msg.sender);
    }

    /**
     * @dev Registers the winner option. This can only be done after the purchaseDeadline is reached
     * and before the registerWinnerDeadline. By registering the option, the game owner can retrieve
     * all ETH collected by tokens sales plus the prize allocated for unsold tokens
     * @param _optionId the unique option id
     */
    // TODO: Improvements - get result from an oracle
    function registerWinner(uint256 _optionId)
        external
        onlyOwner
        optionExists(_optionId) {
            // validations
            require(winnerOptionId == 0, "Winner has already been set");
            require(now >= purchaseDeadline, "Game is still open to bets");
            require(now < registerWinnerDeadline, "Register winner period has ended");
            // effects
            // sets the winner before transferring (prevents reentracy attacks)
            winnerOptionId = _optionId;
            // gets the winner structure...
            Option storage winner = options[_optionId];
            // ...and calculates the total prize
            totalAvailablePrize = winner.totalSupply
                .sub(balances[_optionId][address(this)]) // gets the amount sold
                .mul(prizePerToken);
            // transfers the total collected plus total prize for tokens unsold to the owner
            msg.sender.transfer(address(this).balance.sub(totalAvailablePrize));
            // logs
            emit WinnerRegistered(_optionId, totalAvailablePrize);
    }

    /**
     * @dev Allows winners to withdraw their prize in ETH. The prize is calculated from
     * the sender's balance and prizePerToken
     */
    function claimPrize()
        external {
            // validations
            require(winnerOptionId != 0, "Winner not set");
            // calculates sender's prize
            uint256 totalPrize = balances[winnerOptionId][msg.sender].mul(prizePerToken);
            // burns sender's tokens before transferring (prevents reentrancy attacks)
            balances[winnerOptionId][msg.sender] = 0;
            // transfers prize to sender
            msg.sender.transfer(totalPrize);
            // logs
            emit PrizeClaimed(msg.sender, totalPrize);
    }

    /**
     * @dev "It's every man for himself" function. If game owner didn't register the winner
     * in time, token holders can take back the ETH spent on purchases.
     */
    function redeemTokens(uint256 _optionId)
        external
        optionExists(_optionId) {
            // validations
            require(winnerOptionId == 0, "Cannot reedeem tokens as winner option is set");
            require(now >= registerWinnerDeadline, "Cannot reedeem tokens before register winner period is finished");
            require(balances[_optionId][msg.sender] > 0, "You don't have any tokens of this type");
            // effects
            Option storage option = options[_optionId];
            // calculates invested ETH plus the share in the prize (equally distributed between all players)
            uint256 tokenBalance = balances[_optionId][msg.sender];
            uint256 amount = option.price.mul(tokenBalance) // total paid for tokens
                .add(
                    prizePerToken.mul(maxSupplyPerToken) // total prize sent on game creation...
                    .div(totalTokensSold) // ...get the proportional prize per token
                    .mul(tokenBalance) // ...multiplied by the amount of tokens owned
                );
            // burns all sender's tokens before transferring
            balances[_optionId][msg.sender] = 0;
            // transfers the amount
            msg.sender.transfer(amount);
            // logs
            emit TokensRedeemed(msg.sender, _optionId, tokenBalance, amount);
    }

    /**
     * @dev Internal transfer function. Transfer tokens from a wallet for another. Reverts if
     * sender's balance is insufficient for the transfer
     * @param _optionId the unique option id
     * @param _from the current tokens onwer's address
     * @param _to the beneficiary's address
     * @param _value the amount of tokens to be transferred
     * @return bool whether the transfer was successful
     */
    function _transfer(uint256 _optionId, address _from, address _to, uint256 _value)
        internal
        returns(bool) {
            // effects
            balances[_optionId][_from] = balances[_optionId][_from].sub(_value);
            balances[_optionId][_to] = balances[_optionId][_to].add(_value);
            // logs
            emit Transfer(_optionId, _from, _to, _value);
            return true;
    }

    /**
     * @dev Gets the name of an asset
     * @param _optionId the asset id
     * @return string the name of the asset
     */
    function name(uint256 _optionId) 
        external
        view
        returns (string memory) {
            return options[_optionId].name;
    }

    /**
     * @dev Gets the symbol of an asset
     * @param _optionId the asset id
     * @return string the symbol of the asset
     */
    function symbol(uint256 _optionId)
        external
        view
        returns (string memory) {
            return options[_optionId].symbol;
    }

    /**
     * @dev Gets the decimals of an asset
     * @param _optionId the asset id
     * @return uint8 the decimals of the asset
     */
    function decimals(uint256 _optionId)
        external
        view
        returns (uint8) {
            return options[_optionId].decimals;
    }

    /**
     * @dev Gets the total supply of an asset
     * @param _optionId the asset id
     * @return uint256 the total supply of the asset
     */
    function totalSupply(uint256 _optionId)
        external
        view
        returns (uint256) {
            return options[_optionId].totalSupply;
    }

    /**
     * @dev Checks the balance of a wallet in the asset manager
     * @param _optionId the asset id
     * @param _owner the wallet to be checked
     * @return uint256 the balance
     */
    function balanceOf(uint256 _optionId, address _owner)
        external
        view
        returns (uint256) {
            return balances[_optionId][_owner];
    }

    /**
     * @dev Gets all registered options
     * @return uint256[] the options array
     */
    function index()
        external
        view
        returns (uint256[] memory) {
            return optionsIndex;
    }


}
