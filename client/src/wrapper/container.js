import React, { Component } from 'react'
import { connect } from 'react-redux'
import Web3 from 'web3'

import { creators } from './redux/actions'
import store from '../store'

const {
    web3Connect,
    noWeb3,
    checkAccounts,
    checkNetwork,
    newBlock,
    checkDollar
} = creators

const mapStateToProps = state => {
    return {
        ethereum: state.ethereum.provider,
        account: state.ethereum.account,
        absentWeb3: state.ethereum.absentWeb3,
        networkId: state.ethereum.networkId,
        contracts: state.ethereum.contracts,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        checkWeb3: async () => {
            if (window.ethereum) {
                await window.ethereum.enable()
                const ethereum = new Web3(window.web3.currentProvider)
                dispatch(web3Connect(ethereum))
                const local = new Web3(new Web3.providers.WebsocketProvider('ws://localhost:8545'))
                local.eth.subscribe('newBlockHeaders', (error, block) => {
                    dispatch(newBlock(block))
                })
                return
            }
            return dispatch(noWeb3())
        },
        checkNetwork: () => store.dispatch(checkNetwork()),
        checkAccount: () => store.dispatch(checkAccounts()),
        checkDollar: () => store.dispatch(checkDollar()),
    }
}

class Wrapper extends Component {
    async componentDidMount() {
        await this.props.checkWeb3()
        this.props.checkNetwork()
        this.props.checkAccount()
        this.props.checkDollar()
        this.poll = setInterval(() => {
            this.props.checkNetwork()
            this.props.checkAccount()
            this.props.checkDollar()
        }, 1000)
    }
    componentWillUnmount() {
        clearInterval(this.poll)
    }
    render() {
        if (this.props.absentWeb3) return <p>You'll need metamask to interact with this application</p>
        if (!this.props.account) return <p>Please login into metamask</p>
        return this.props.children
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Wrapper)