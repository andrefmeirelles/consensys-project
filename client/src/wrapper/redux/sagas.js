import { call, put, takeLatest, select } from 'redux-saga/effects'
import _ from 'lodash'
import { creators } from './actions'
import gameBuildSpecs from '../../contracts/Game.json'

const {
    accountChange,
    networkChange,
    noWeb3,
    contractsLoaded,
    dollarFetched
} = creators

const selectors = {
    getAccount: state => state.ethereum.account,
    getProvider: state => state.ethereum.provider,
    getNetworkId: state => state.ethereum.networkId,
    getContracts: state => state.ethereum.contracts,
    getDollar: state => state.ethereum.dollar,
}

const API = () => ({
    fetchDollar: async () => {
        const response = await fetch('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=USD')
        return _.get(await response.json(), 'USD')
    }
})

export function* getAccounts() {
    try {
        const ethereum = yield select(selectors.getProvider)
        if (!ethereum) throw Error('No web3')
        const stateAccount = yield select(selectors.getNetworkId)
        const accounts = yield call(ethereum.eth.getAccounts)
        if (stateAccount !== accounts[0]) yield put(accountChange(accounts[0]))
    } catch (e) {
        console.log('getaccounts', e)
        yield put(noWeb3())
    }
}

export function* getNetworkAndContracts() {
    try {
        const ethereum = yield select(selectors.getProvider)
        if (!ethereum) throw Error('No web3')
        const stateNetworkId = yield select(selectors.getNetworkId)
        const networkId = yield call(ethereum.eth.net.getId)
        if (stateNetworkId !== networkId) {
            yield put(networkChange(networkId))
            yield loadContracts(ethereum, networkId)
        }
    } catch (e) {
        console.log('getnetwork', e)
        yield put(noWeb3())
    }
}

export function* loadContracts(provider, networkId) {
    try {
        const contracts = yield select(selectors.getContracts)
        const { contractName, abi, deployedBytecode, networks } = gameBuildSpecs
        const { address } = networks[networkId]
        yield put(contractsLoaded({
            ...contracts,
            [contractName]: {
                abi,
                deployedBytecode,
                address,
                instance: new provider.eth.Contract(abi, address)
            }
        }))
    } catch (e) {
        console.log('loadContracts', e)
        // TODO: display error message
    }
}

export function* checkDollar() {
    const dollar = yield call(API().fetchDollar)
    yield put(dollarFetched(dollar))
}

export function* getAccountsSaga() {
    yield takeLatest('CHECK_ACCOUNTS', getAccounts)
}

export function* getNetworkAndContractsSaga() {
    yield takeLatest('CHECK_NETWORK', getNetworkAndContracts)
}

export function* updateDollarSaga() {
    yield takeLatest('FETCH_DOLLAR', checkDollar)
}