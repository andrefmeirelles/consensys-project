import { actions } from './actions'
const {
    WEB3_CONNECT,
    WEB3_ACCOUNT_CHANGE,
    NO_WEB3,
    WEB3_NETWORK_CHANGE,
    CONTRACTS_LOADED,
    DOLLAR_FETCHED
} = actions

const initialState = {
    provider: null,
    absentWeb3: false,
    account: null,
    networkId: null,
    contracts: {}
}

const ethereum = (state = initialState, action) => {
    switch (action.type) {
        case WEB3_CONNECT:
            return {
                ...state,
                provider: action.ethereum
            }
        case NO_WEB3:
            return {
                ...state,
                absentWeb3: true
            }
        case WEB3_ACCOUNT_CHANGE:
            return {
                ...state,
                account: action.account,
            }
        case WEB3_NETWORK_CHANGE:
            return {
                ...state,
                networkId: action.networkId,
            }
        case CONTRACTS_LOADED:
            return {
                ...state,
                contracts: action.contracts,
            }
        case DOLLAR_FETCHED:
                return {
                    ...state,
                    dollar: action.dollar,
                }
        default:
            return state
    }
}

export default ethereum