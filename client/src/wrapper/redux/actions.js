// Actions
const actions = {
    WEB3_CONNECT: 'WEB3_CONNECT',
    NO_WEB3: 'NO_WEB3',
    CHECK_ACCOUNTS: 'CHECK_ACCOUNTS',
    WEB3_ACCOUNT_CHANGE: 'WEB3_ACCOUNT_CHANGE',
    CHECK_NETWORK: 'CHECK_NETWORK',
    WEB3_NETWORK_CHANGE: 'WEB3_NETWORK_CHANGE',
    CONTRACTS_LOADED: 'CONTRACTS_LOADED',
    NEW_BLOCK: 'NEW_BLOCK',
    FETCH_DOLLAR: 'FETCH_DOLLAR',
    DOLLAR_FETCHED: 'DOLLAR_FETCHED',
}

// Action creators
const creators = {
    web3Connect: ethereum => ({ type: actions.WEB3_CONNECT, ethereum }),
    noWeb3: () => ({ type: actions.NO_WEB3 }),
    newBlock: () => ({ type: actions.NEW_BLOCK }),
    checkAccounts: () => ({ type: actions.CHECK_ACCOUNTS }),
    checkNetwork: () => ({ type: actions.CHECK_NETWORK }),
    networkChange: networkId => ({ type: actions.WEB3_NETWORK_CHANGE, networkId }),
    accountChange: account => ({ type: actions.WEB3_ACCOUNT_CHANGE, account }),
    contractsLoaded: contracts => ({ type: actions.CONTRACTS_LOADED, contracts }),
    checkDollar: () => ({ type: actions.FETCH_DOLLAR }),
    dollarFetched: dollar => ({ type: actions.DOLLAR_FETCHED, dollar }),
}

export {
    actions,
    creators
}