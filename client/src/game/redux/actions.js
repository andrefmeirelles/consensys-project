// Actions
const actions = {
    FETCH_OPTIONS: 'FETCH_OPTIONS',
    GAME_PROPS: 'GAME_PROPS',
    OPTIONS_FETCHED: 'OPTIONS_FETCHED',
    FETCH_DOLLAR: 'FETCH_DOLLAR',
    DOLLAR_FETCHED: 'DOLLAR_FETCHED',
    BUY_TOKEN_REQUEST: 'BUY_TOKEN_REQUEST',
    CLEAR_INTENTION: 'CLEAR_INTENTION',
    TRANSACTION_SENT: 'TRANSACTION_SENT',
    TRANSACTION_MINED: 'TOKEN_BOUGHT',
    SET_TOKEN_AMOUNT: 'SET_TOKEN_AMOUNT'
}

// Action creators
const creators = {
    gameProps: data => ({ type: actions.GAME_PROPS, data }),
    fetchOptions: () => ({ type: actions.FETCH_OPTIONS }),
    optionsFetched: options => ({ type: actions.OPTIONS_FETCHED, options }),
    fetchDollar: () => ({ type: actions.FETCH_DOLLAR }),
    dollarFetched: dollar => ({ type: actions.DOLLAR_FETCHED, dollar }),
    setPurchaseIntention: (purchase, amount) => ({ type: actions.SET_TOKEN_AMOUNT, intention: { purchase, amount } }),
    clearIntention: () => ({ type: actions.CLEAR_INTENTION }),
    buyTokens: () => ({ type: actions.BUY_TOKEN_REQUEST }),
    transactionSent: pending => ({ type: actions.TRANSACTION_SENT, pending }),
    transactionMined: hash => ({ type: actions.TRANSACTION_MINED, hash }),
}

export {
    actions,
    creators
}