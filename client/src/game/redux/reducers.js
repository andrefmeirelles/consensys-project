import _ from 'lodash'
import { actions } from './actions'

const {
    GAME_PROPS,
    OPTIONS_FETCHED,
    SET_TOKEN_AMOUNT,
    TRANSACTION_SENT,
    CLEAR_INTENTION,
    TRANSACTION_MINED,
} = actions

const initialGameState = {
    options: [],
        dollar: 0
}

const game = (state = initialGameState, action) => {
    switch (action.type) {
        case GAME_PROPS: 
            return {
                ...state,
                data: action.data
            }
        case OPTIONS_FETCHED:
            return {
                ...state,
                options: action.options
            }
        default:
            return state
    }
}

const initialPurchaseState = {
    intention: {
        amount: 0
    },
    pendingTransactions: []
}

const purchase = (state = initialPurchaseState, action) => {
    switch (action.type) {
        case SET_TOKEN_AMOUNT:
            return {
                ...state,
                intention: action.intention
            }
        case CLEAR_INTENTION:
            return {
                ...state,
                intention: { amount: 0 }
            }
        case TRANSACTION_SENT:
            return {
                ...state,
                pendingTransactions: state.pendingTransactions.concat([action.pending])
            }
        case TRANSACTION_MINED:
            const found = _.find(state.pendingTransactions, { hash: action.hash })
            return {
                ...state,
                pendingTransactions: _.without(state.pendingTransactions, found)
            }
        default:
            return state
    }
}

export { game, purchase }