import { call, put, takeEvery, takeLatest, select } from 'redux-saga/effects'
import { creators } from './actions'
import _ from 'lodash'
import Decimal from 'decimal.js'

import { snackbarActions as snackbar } from 'material-ui-snackbar-redux'

const { gameProps, optionsFetched, transactionSent, clearIntention, transactionMined } = creators

const API = instance => ({
    getIndex: async () => await instance.methods.index().call(),
    getGameProps: async () => Promise.all(
        [
            await instance.methods.gameName().call(),
            await instance.methods.prizePerToken().call(),
            await instance.methods.winnerOptionId().call(),
            await instance.methods.purchaseDeadline().call(),
        ]
    ),
    getOptions: ids => Promise.all(
        _.map(ids, async id => await instance.methods.options(id).call())
    ),
    getSupply: (ids, contractAddress) => Promise.all(
        _.map(ids, async id => ({ id, supply: await instance.methods.balanceOf(id, contractAddress).call() }) )
    ),
    buyTokens: async (id, value, account) =>
        await instance.methods.buyTokens(id).send({
            value: value.toString(),
            from: account
        }),
    getTransaction: async hash => await instance.eth.getTransactionReceipt(hash)
})

const selectors = {
    getProvider: state => state.ethereum.provider,
    getContracts: state => state.ethereum.contracts,
    getAccount: state => state.ethereum.account,
    getIntention: state => state.purchase.intention,
    getPendingTx: state => state.purchase.pendingTransactions
}

export function* loadGame() {
    try {
        const { Game } = yield select(selectors.getContracts)
        const api = API(Game.instance)
        // get game props
        const [ name, prizePerToken, winner, purchaseDeadline ] = yield call(api.getGameProps)
        yield put(gameProps({ name, prizePerToken, winner, purchaseDeadline }))
        // get options data
        const ids = yield call(api.getIndex)
        const options = yield call(api.getOptions, ids)
        const supplies = yield call(api.getSupply, ids, Game.address)
        yield put(optionsFetched(
            _.orderBy(
                _.map(options, opt => ({
                ..._.pick(opt, [ 'id', 'decimals', 'name', 'symbol', 'totalSupply', 'price', 'token' ]),
                currentSupply: _.get(_.find(supplies, { id: opt.id }), 'supply'),
                })), 'name'
            )
        ))
    } catch (e) {
        console.log('loadGame', e)
        // TODO: display load error
    }
}

export function* buyToken() {
    // get web3
    const { utils } = yield select(selectors.getProvider)
    // get contract instance
    const { Game } = yield select(selectors.getContracts)
    const api = API(Game.instance)
    // get current account
    const account = yield select(selectors.getAccount)
    // get purchase data
    const { purchase, amount } = yield select(selectors.getIntention)
    // buy'em
    const ethValue = utils.toWei(new Decimal(purchase.priceInETH).mul(amount).toString(), 'ether')
    try {
        yield put(snackbar.show({ message: `Sending transaction...` }))
        const tx = yield call(api.buyTokens, purchase.id, ethValue, account)
        // store transaction to be monitored
        yield put(transactionSent({ ...purchase, hash: tx.transactionHash, amount }))
        // show message
        yield put(snackbar.show({ message: `Transaction sent. It can take several minutes until the transaction gets mined. Please, wait.` }))
    } catch (error) {
        console.log('buytoken error', error)
        // TODO: display rejection/errors
    }
    // clear intention
    yield put(clearIntention())
}

export function* checkPending() {
    const pending = yield select(selectors.getPendingTx)
    if (_.isEmpty(pending)) return
    const ethereum = yield select(selectors.getProvider)
    const api = API(ethereum)
    try {
        for (const tx of pending) {
            const receipt = yield call(api.getTransaction, tx.hash)
            console.log(receipt)
            if (receipt.status) {
                yield put(snackbar.show({ message: `Your purchase of ${tx.amount} ${tx.symbol} token(s) was successfully mined. Check your wallet.` }))
                yield put(transactionMined(receipt.transactionHash))
            }
        }
    } catch (error) {
        console.log('checkpending', error)
    }
}


export function* loadGameSaga() {
    yield takeLatest([ 'CONTRACTS_LOADED', 'NEW_BLOCK' ], loadGame)
}

export function* buyTokenSaga() {
    yield takeEvery([ 'BUY_TOKEN_REQUEST' ], buyToken)
}

export function* checkPendingTransactionsSaga() {
    yield takeEvery([ 'NEW_BLOCK' ], checkPending)
}