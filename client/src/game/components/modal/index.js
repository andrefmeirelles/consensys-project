import React, { Component } from 'react'
import Rodal from 'rodal'
import ButtonGroup from '@material-ui/core/ButtonGroup'
import Button from '@material-ui/core/Button'
import AddIcon from '@material-ui/icons/Add'
import RemoveIcon from '@material-ui/icons/Remove'
import { connect } from 'react-redux'

import { creators } from '../../redux/actions'
import './styles/rodal.css'
import 'rodal/lib/rodal.css'

const { buyTokens, setPurchaseIntention, clearIntention } = creators

const mapStateToProps = state => {
    return {
        game: state.game.data,
        dollar: state.ethereum.dollar,
        fromWei: state.ethereum.provider.utils.fromWei,
        amount: state.purchase.intention.amount
    }
}

const mapDispatchToProps = dispatch => {
    return {
        buyTokens: () => dispatch(buyTokens()),
        setPurchaseIntention: (id, amount, price) => dispatch(setPurchaseIntention(id, amount, price)),
        clearIntention: () => dispatch(clearIntention()),
    }
}

class PurchaseModal extends Component {
    constructor(props) {
        super(props)
        this.onClose = this.onClose.bind(this)
    }
    onClose() {
        this.props.onClose()
        this.props.clearIntention()
    }
    render() {
        const { selected, amount, game, fromWei, dollar } = this.props
        const prizePerToken = Number(fromWei(game.prizePerToken.toString()))
        return (
            <Rodal visible={this.props.showModal} onClose={() => {
                this.setState({ purchasing: false })
                this.props.onClose()
            }}>
                { selected ?
                    <div>
                        <h3>Buy {selected.name} tokens</h3>
                        <div className="amounts">
                            <span>Available: {selected.currentSupply - amount}</span>
                            <div className="tokens">
                                <span>Amount:</span>
                                <span className="amount">{amount}</span>
                                
                                <ButtonGroup variant="contained" size="small">
                                    <Button
                                        variant="contained"
                                        disabled={selected.currentSupply - amount <= 0}
                                        onClick={() => {
                                            if (selected.currentSupply - amount > 0) this.props.setPurchaseIntention(selected, amount + 1)}
                                        }
                                        ><AddIcon /></Button>
                                    <Button
                                        variant="contained"
                                        disabled={amount <= 0}
                                        onClick={() => {
                                            if (amount > 0) this.props.setPurchaseIntention(selected, amount - 1)}
                                        }
                                    ><RemoveIcon /></Button>
                                </ButtonGroup>
                            </div>
                        </div>
                        <div>
                            <div className="values">
                                <span>Token: {selected.symbol} - {selected.name} </span>
                                <span>Predicted prize:
                                    <span className="eth">
                                        {(prizePerToken * amount).toFixed(4)} ETH
                                    </span>
                                    <span className="usd">
                                        (${(prizePerToken * dollar * amount).toFixed(2)})
                                    </span>
                                </span>
                                <span>Price: 
                                    <span className="eth">
                                        {(selected.priceInETH * amount).toFixed(4)} ETH
                                    </span>
                                    <span className="usd">
                                        (${(selected.priceInUSD * amount).toFixed(2)})
                                    </span>
                                </span>
                            </div>
                        </div>
                        <div className='footer'>
                            <Button
                                variant="contained" 
                                onClick={this.onClose}>
                                Cancel
                            </Button>
                            <Button
                                variant="contained" 
                                color="primary"
                                disabled={amount === 0}
                                onClick={() => {
                                    this.props.buyTokens()
                                    this.onClose()
                                }}>
                                Buy
                            </Button>
                        </div>
                    </div> :
                    <p>loading...</p>
                }
            </Rodal>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PurchaseModal)