import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from 'lodash'
import moment from 'moment'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import Paper from '@material-ui/core/Paper'
import Button from '@material-ui/core/Button'
import Tooltip from '@material-ui/core/Tooltip'
import { CopyToClipboard } from 'react-copy-to-clipboard'
import { snackbarActions as snackbar } from 'material-ui-snackbar-redux'

import PurchaseModal from '../modal'

import './styles/options.css'

const mapStateToProps = state => {
    return {
        ethereum: state.ethereum.provider,
        account: state.ethereum.account,
        options: state.game.options,
        dollar: state.ethereum.dollar,
        purchaseDeadline: state.game.data.purchaseDeadline,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        snackbar: msg => dispatch(snackbar.show(msg))
    }
}

class OptionsTable extends Component {
    state = { showModal: false }
    render() {
        const { options, ethereum, dollar, purchaseDeadline } = this.props
        const deadlineReached = moment().isSameOrAfter(moment(purchaseDeadline, 'X'))
        const opts = _.map(options, opt => {
                const priceInETH = Number(ethereum.utils.fromWei(opt.price, 'ether'))
                const priceInUSD = priceInETH * dollar
                return {
                    ...opt,
                    priceInETH,
                    priceInUSD
                }
            })
        return (
            deadlineReached ?
            <h1>Selling period has ended</h1> :
            <div>
                <PurchaseModal onClose={() => this.setState({ showModal: false })} {...this.state}/>
                <Paper className="root">
                    <Table className="table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Option</TableCell>
                                <TableCell align="right">Token Symbol</TableCell>
                                <TableCell align="right">Price in ETH</TableCell>
                                <TableCell align="right">Price in USD</TableCell>
                                <TableCell align="right">Total Supply</TableCell>
                                <TableCell align="right">Available</TableCell>
                                <TableCell align="right">Address</TableCell>
                                <TableCell align="right">Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {opts.map(opt => (
                                <TableRow key={opt.id}>
                                    <TableCell component="th" scope="row">
                                        {opt.name}
                                    </TableCell>
                                    <TableCell align="right">{opt.symbol}</TableCell>
                                    <TableCell align="right">{opt.priceInETH}</TableCell>
                                    <TableCell align="right">{opt.priceInUSD.toFixed(2)}</TableCell>
                                    <TableCell align="right">{opt.totalSupply}</TableCell>
                                    <TableCell align="right">{opt.currentSupply}</TableCell>
                                    <TableCell align="right">
                                        <Tooltip title="Click to copy to clipboard" >
                                            <CopyToClipboard
                                                text={opt.token}
                                                onCopy={() => this.props.snackbar({ message: 'Token address copied to clipboard'})} >
                                                <span className="copy">{_.truncate(opt.token, { length: 16 })}</span>
                                            </CopyToClipboard>
                                            </Tooltip>
                                    </TableCell>
                                    <TableCell align="right">
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            onClick={() => this.setState({ showModal: true, selected: opt })}>
                                            Buy
                                        </Button>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OptionsTable)