import React from 'react'
import { connect } from 'react-redux'
import moment from 'moment'

import './styles/header.css'

const mapStateToProps = state => {
    return {
        fromWei: state.ethereum.provider.utils.fromWei,
        account: state.ethereum.account,
        ...state.game.data,
    }
}

const header = props =>(
    <div className="header">
        <h1 className="header-title">{props.name}</h1>
        <span className="header-subtitle">
            Bets end: {moment(props.purchaseDeadline, 'X').format('DD/MMM/YY HH:mm:SS')} |
            Prize per token: {props.prizePerToken && props.fromWei(props.prizePerToken.toString(), 'ether')} ETH
        </span>
        <span className="header-subtitle">Connected account: {props.account}</span>
    </div>
)

export default connect(mapStateToProps)(header)