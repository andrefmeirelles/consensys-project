import React, { Component } from 'react'
import { connect } from 'react-redux'
import { creators } from './redux/actions'
import _ from 'lodash'
import CircularProgress from '@material-ui/core/CircularProgress';
import Options from './components/options'
import Header from './components/header'

import Container from '@material-ui/core/Container'

const { fetchOptions } = creators

const mapStateToProps = state => {
    return {
        dollar: state.ethereum.dollar,
        options: state.game.options,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getOptions: () => dispatch(fetchOptions())
    }
}

class GameContainer extends Component {
    state = { dollar: null }
    async componentDidMount() {
        this.props.getOptions()
    }
    render() {
        return <Container>
            <Header></Header>
            {
                _.isEmpty(this.props.options) ?
                    <CircularProgress /> :
                    this.props.dollar && <Options {...this.props} />
            }
        </Container>
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GameContainer)