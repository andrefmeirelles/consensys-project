import React from 'react'
import { SnackbarProvider } from 'material-ui-snackbar-redux'
import './App.css'
import Wrapper from './wrapper/container'
import GameContainer from './game/container'

export default props => (
    <Wrapper>
        <SnackbarProvider SnackbarProps={{ autoHideDuration: 4000, anchorOrigin: { vertical: 'top', horizontal: 'right' } }} >
            <GameContainer />
        </SnackbarProvider>
    </Wrapper>
)
