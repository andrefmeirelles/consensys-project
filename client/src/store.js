import { createStore, applyMiddleware } from 'redux'
import { combineReducers } from 'redux'
import { snackbarReducer } from 'material-ui-snackbar-redux'
import createSagaMiddleware from 'redux-saga'

// reducers
import ethereum from './wrapper/redux/reducers'
import { game, purchase } from './game/redux/reducers'

// sagas
import { getAccountsSaga, getNetworkAndContractsSaga, updateDollarSaga } from './wrapper/redux/sagas'
import { loadGameSaga, buyTokenSaga, checkPendingTransactionsSaga } from './game/redux/sagas'

const sagaMiddleware = createSagaMiddleware()

export default createStore(
    combineReducers({ ethereum, game, purchase, snackbar: snackbarReducer }),
    applyMiddleware(sagaMiddleware)
)

// function* composeSagas() {
//     return yield all([
//         getAccountsSaga(),
//         getNetworkAndContractsSaga(),
//         gameSaga()
//     ])
// }

sagaMiddleware.run(getAccountsSaga)
sagaMiddleware.run(getNetworkAndContractsSaga)
sagaMiddleware.run(updateDollarSaga)
sagaMiddleware.run(loadGameSaga)
sagaMiddleware.run(buyTokenSaga)
sagaMiddleware.run(checkPendingTransactionsSaga)